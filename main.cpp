#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <queue>
#include <time.h>
#include "customer.hpp"
#include "worker.hpp"
#include <unistd.h>

void *customer(void*); //customer thread
void *worker(void*); //worker thread

const int CUSTOMERS = 50;
const int WORKERS = 3;
const int MAX_CAPACITY = 10;

sem_t max_capacity; //max allowd in office
sem_t worker_avail; //is worker available
sem_t scale; //is scale available
sem_t mutex1; //queing/dequeing critical section
sem_t customer_done; //display finished task before getting next worker
sem_t finished[CUSTOMERS]; //telling customer its finished

std::queue<Customer> postalQueue; //queue waiting for postal worker

int cids[CUSTOMERS];
int wids[WORKERS];


pthread_t customers[CUSTOMERS];
pthread_t workers[WORKERS];

int result; //result when creating/joing thread

int main() {

//initaling semaphores
sem_init(&max_capacity,0,MAX_CAPACITY); //max 10 in office at a time
sem_init(&worker_avail,0,WORKERS); //max 3 workers availabe at a time
sem_init(&scale,0,1); //scale can only be used by 1 worker at a time
sem_init(&mutex1,0,1); //acessing queue one at a time
sem_init(&customer_done,0,3); 
for (int i=0; i <CUSTOMERS; i++)
{
	sem_init(&finished[i],0,0);
}
std::cout << "Simulating Post Office with 50 customers and 3 postal workers" << std::endl;

srand(time(NULL)); //set seed for randomly assigning tasks

//creating threads for customers
for (int n = 0; n < CUSTOMERS; n++) 
{
	// std::cout << "Creating  customer threads\n";
	cids[n] = n;
	result = pthread_create(&customers[n],NULL,customer,&cids[n]);
	usleep(100);
	if (result) {
		std::cout << "Customer thread creation error! " << std::endl;
		exit(1);
	}
}
//creating the 3 threads for workers
for (int n = 0; n < WORKERS; n++) {
		wids[n] = n;
		
		result = pthread_create(&workers[n],NULL,worker,&wids[n]);
		usleep(100); 
		if (result) {
			std::cout << "Worker thread creation error! " << std::endl;
			exit(1);
		}
}


//joing threads
for (int n = 0; n < CUSTOMERS; n++) {
	result = pthread_join(customers[n],NULL);
	std::cout << "Joined customer " << n << std::endl;
	if (result) {
		std::cout << "join error!" <<std::endl;
		exit(1);
	}
}

for (int n = 0; n < WORKERS; n++) {
	result = pthread_join(workers[n], NULL);
	std::cout << "Joined postal worker " << n << std::endl;
	if (result) {
		std::cout << "join error!" << std::endl;
		exit(1);
	}
}

return 0;
}

void *customer (void* arg) {
	int cust_id = *(int *) arg;
	Customer customer(cust_id);
	
	sem_wait(&max_capacity); //wait for room to go into post office
	sem_wait(&mutex1); //wait for queue resource
		customer.enter_postoffice();
		postalQueue.push(customer); //queue access 1 thread at a time
	sem_post(&mutex1); // release queue resource
	sem_wait(&worker_avail); //wait for a worker to be available
	sem_wait(&finished[customer.get_id()]); //wait for worker to be done serving
		customer.finish_task();
	sem_post(&customer_done);
		customer.leave_postoffice(); 
	sem_post(&max_capacity);

	
	return arg;
}

void *worker (void* arg) {
	int work_id = *(int *) arg;
	PostalWorker worker(work_id);
	while (!postalQueue.empty()) {
		sem_post(&worker_avail); //tell next customer in line its his turn
		sem_wait(&mutex1); //pop from queue one at a time
		worker.get_next_customer(postalQueue.front()); //get next customer
		postalQueue.pop(); //remove customer from queue now at postal worker
		sem_post(&mutex1);
		int task = worker.get_task();
		if (task == PACKAGE_TASK) { //need to wait if scale in use
			sem_wait(&scale);
			worker.use_scale();
			worker.run_task();
			sem_post(&scale);
		}
		else {
			worker.run_task();
		}
		sem_post(&finished[worker.get_customer_id()]); 
		sem_wait(&customer_done);
	}


	return arg;
}