#ifndef POSTAL_WORKER_H
#define POSTAL_WORKER_H

#include "customer.hpp"




static int const PACKAGE_TASK = 0;
static int const LETTER_TASK = 1;
static int const STAMP_TASK = 2;
static int const STAMP_TIME = 1000000;
static int const LETTER_TIME = 1500000;
static int const PACKAGE_TIME = 2000000;

class PostalWorker
{
	private:
		int id;
		Customer *customer; //will hold address of a customer from queue
	
	public:
		PostalWorker(int);
		void get_next_customer(Customer&);
		int get_customer_id();
		int get_task();
		void run_task();
		void use_scale();

		
};


#endif