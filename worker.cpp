#include "worker.hpp"
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

PostalWorker::PostalWorker(int id): id(id), customer(NULL)
{
	std::cout << "Postal worker " << this->id << " created\n";
}

void PostalWorker::get_next_customer(Customer &cust)
{
	this->customer = &cust;
	std::cout << "Postal worker " << this->id << " serving Customer " << this->customer->get_id() << std::endl;
}

int PostalWorker::get_task()
{
	int task = customer->get_task();
	int cid = customer->get_id();

	//displaying requested task
	std::string disString("");
	disString += ("Customer " + std::to_string((long long int)(cid)) + " asks postal worker " + \
		std::to_string((long long int)(this->id)) + " to ");
	
	if (task == PACKAGE_TASK){
		disString += "send a package";
	}
	else if (task == LETTER_TASK) {
		disString += "send a letter";
	}
	else {
		disString += "buy stamps";
	}

	std::cout << disString << std::endl;

	return task;
}

void PostalWorker::run_task()
{
	int task = this->customer->get_task();
	std::string disString("Postal worker ");
	disString += (std::to_string((long long int)(this->id)) + " finished serving Customer " + std::to_string((long long int)(this->customer->get_id())));
	
	if (task == STAMP_TASK) {
		usleep(STAMP_TIME);
	}
	else if (task == LETTER_TASK ) {
		usleep(LETTER_TIME);
	}
	else {
		usleep(PACKAGE_TIME); 
	}
	
	std::cout << disString << std::endl;
}

int PostalWorker::get_customer_id()
{
	return this->customer->get_id();
}

void PostalWorker::use_scale()
{
	std::cout << "Scale in use by postal worker " << this->id << std::endl;
}