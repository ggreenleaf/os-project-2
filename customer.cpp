#include "customer.hpp"
#include <stdlib.h> //rand() for setting task
#include <iostream>
#include "worker.hpp" //get TASK_NUMBERS

Customer::Customer(int id): id(id), task(rand() % 3)
{
	std::cout << "Customer " << this->id << " created.\n";	
}

void Customer::enter_postoffice()
{
	std::cout << "Customer " << this->id << " entering post office\n";
}

int Customer::get_id() 
{
	return this->id;
}
int Customer::get_task()
{
	return this->task;
}
void Customer::finish_task()
{
	std::string  disString("Customer ");
	disString += (std::to_string((long long int)(this->id)) + " finished ");
	if (this->task == PACKAGE_TASK ) {
		disString += "mailing a package.";
	}
	else if (this->task == LETTER_TASK) {
		disString += "sending a letter";
	}
	else {
		disString += "buying stamps";
	}
	std::cout << disString << std::endl;
}
void Customer::leave_postoffice()
{
	std::cout << "Customer " << this->id << " leaves post office" << std::endl;
}



