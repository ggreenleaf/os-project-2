Geoffrey Greenleaf
Project 2 Post Office Threads and Semaphores


compiling the project
	a make file will be provided in the project directory to compile the program
	the command is make
		$ make
Running the project
	after running make the out file should be project2.out to run the program type the command
	
		$./project2.out

example of compiling and running the project after downloading zip file in terminal

```
#!bash


user@host dir $ unzip project2.zip
...
user@host dir $ cd project2; make
g++ -std=c++0x -g  -c customer.cpp worker.cpp main.cpp -lpthread -lrt
g++ -std=c++0x -g  -o project2.out customer.o worker.o main.o -lpthread -lrt
user@host project2 $ ./project2.out
Simulating Post Office with 50 customers and 3 postal workers
Customer 0 created.
Customer 0 entering post office
Customer 1 created.
Customer 1 entering post office
Customer 2 created.
Customer 2 entering post office
...
Joined postal worker 2
user@host project2 $
```