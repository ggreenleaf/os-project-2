#declaring variables
CC=g++
CXXFLAGS = -std=c++0x -g 

objects = customer.o worker.o main.o
sources = customer.cpp worker.cpp main.cpp

project2.out: $(objects)
	$(CC) $(CXXFLAGS) -o project2.out $(objects) -lpthread -lrt

customer.o: customer.hpp
worker.o: worker.hpp
main.o: customer.hpp worker.hpp

$(objects): $(sources)
		$(CC) $(CXXFLAGS) -c $(sources) -lpthread -lrt
clean:
	rm $(objects) project2.out
