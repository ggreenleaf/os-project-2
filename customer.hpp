#ifndef CUSTOMER_H
#define CUSTOMER_H


class Customer
{
	private:
		int id;
		int task;

	
	public:
		Customer(int);
		// ~Customer();
		int get_id();
		int get_task();
		void enter_postoffice();
		void finish_task();
		void leave_postoffice();
		void set_postal_worker_id();
};		






#endif